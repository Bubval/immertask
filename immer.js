const produce = (baseState, callback) => {
    const draftState = clone(baseState);
    callback(draftState);
    return draftState;
};

const clone = (obj) => {
    var copy;

    if (obj === null || typeof obj !== 'object') return obj;

    if (obj instanceof Array) {
        copy = [];
        for (var i = 0; i < obj.length; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = clone(obj[attr]);
            }
        }
        return copy;
    }

    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    throw new Error("Cannot clone.")
}

const baseState = [
    {
        title: "Learn TypeScript",
        done: true
    },
    {
        title: "Try Immer",
        done: false
    }
]

const nextState = produce(baseState, (draft) => {
    draft[1].done = true
    draft.push({title: "Tweet about it"})
})

console.log(baseState);
console.log(nextState);